<?php

namespace App\Http\Controllers\Admin;


use App\Http\Requests\Admin\StoreClient_Pay;
use App\Http\Requests\Admin\StoreClientOrder;
use App\Http\Requests\Admin\StoreClientPay;
use App\Http\Requests\Admin\StoreClientProfile;
use App\Models\FuelPriceNew;
use App\Models\Pay;
use App\Models\ViewBalance;
use App\Http\Requests\Admin\StoreClientFind;
use App\Models\Order;
use App\Models\RefDeliveryKind;
use App\Models\RefPayKind;
use App\Models\RefFuelKind;
use App\Models\RefProduct;
use App\Models\RefStatus;
use App\Models\User;
use App\Models\UserFuelCard;
use App\Models\UserPhone;
use App\Http\Controllers\Controller;
use App\Models\ViewCientFind;
use App\Models\ViewOrder;
use App\Models\ViewPay;
use Illuminate\Http\Request;
use Facades\App\Facades\PhoneFacade;

class ClientController extends Controller
{
    public function index()
    {
        return view('admin.main.index', compact('result'));
    }

    public function clientFind(StoreClientFind $request)
    {

        $keyWord = $request->key_word;
        $type = $request->type;


        switch ($type) {
            case 'login':
            case 'name':

                $findUser = ViewCientFind::where($type, 'like', "%{$keyWord}%")->get();
                $backIcon = True;
                return view('admin.clients.clientsFind.index',
                    compact('findUser', 'keyWord', 'type', 'backIcon'));
                break;

            case 'card':
                $backIcon = True;
                $findUser = ViewCientFind::where('card_fuel', 'like', "%{$keyWord}%")->get();
                return view('admin.clients.clientsFind.index',
                    compact('findUser', 'keyWord', 'type', 'backIcon'));
                break;

            case 'payer':
                $backIcon = True;

                $userArr = ViewPay::where('payer', 'like', "%{$keyWord}%")->get()->pluck('user_id');
                $findUser = ViewCientFind::whereIn('id', $userArr)->get();

                return view('admin.clients.clientsFind.index',
                    compact('findUser', 'keyWord', 'type', 'backIcon'));
                break;
        }


        $findUser = ViewCientFind::all();

        return view('admin.clients.clientsFind.index', compact('findUser', 'keyWord', 'type'));
    }

    public function clientProfile(Request $request, $id)
    {
        $refArr['refStatus'] = RefStatus::all()->pluck('title', 'id');
        $refArr['refProduct'] = RefProduct::all()->pluck('title', 'id');
        $refArr['refFuelKind'] = RefFuelKind::all()->pluck('title', 'id');

        $userProfile = User::with('userPhone', 'userFuelCard')->find($id);

        if (ViewOrder::where('user_id', $id)) {
            $result = ViewOrder::where('user_id', $id)->latest()->paginate(10);
        }

        $balance = ViewBalance::where('user_id', $id)->first();

        $refArrJSON = json_encode($refArr, JSON_UNESCAPED_UNICODE);
        $userProfile = json_encode($userProfile, JSON_UNESCAPED_UNICODE);

        return view('admin.clients.clientsFind.clientsProfile.index',
            compact('userProfile', 'result', 'id', 'balance', 'refArrJSON', 'refArr'))
            ->with('i', ($request->input('page', 1) - 1) * 10);

    }

    public function clientUpdate(StoreClientProfile $request, $id)
    {
        $coworker = $request->coworker ? 1 : 0;
        $admin = $request->admin ? 1 : 0;

        User::whereIn('admin', ['1', '0'])->where('id', '=', $id)->update([
            'name' => $request->name,
            'login' => PhoneFacade::trim($request->login),
            'password' => bcrypt($request->password),
            'email' => $request->email,
            'info' => $request->info,
            'coworker' => $coworker,
            'admin' => $admin,
        ]);

        if ($request->fuel) {
            $countFuel = count($request->fuel);

            //Удаление карт
            $UserFuelCard = UserFuelCard::where('user_id', $id)->pluck('id')->toArray();

            $idFuelCard = $request->idFuelCard;

            $delete = array_diff($UserFuelCard, $idFuelCard);

            UserFuelCard::destroy($delete);


            //добавление карт
            for ($i = 0; $i < $countFuel; $i++) {
                if (UserFuelCard::find($request->idFuelCard[$i])) {
                    UserFuelCard::find($request->idFuelCard[$i])->update([
                        'user_id' => $id,
                        'fuel' => $request->fuel[$i],
                        'card' => $request->card[$i],
                    ]);
                } else {
                    UserFuelCard::Create([
                        'user_id' => $id,
                        'fuel' => $request->fuel[$i],
                        'card' => $request->card[$i],
                    ]);
                }
            }
        }
        if ($request->phone) {
            $countTel = count($request->phone);
            for ($i = 0; $i < $countTel; $i++) {
                if (UserPhone::find($request->idPhone[$i])) {
                    UserPhone::find($request->idPhone[$i])->update([
                        'user_id' => $id,
                        'phone' => $request->phone[$i],
                    ]);
                } else {
                    UserPhone::Create([
                        'user_id' => $id,
                        'phone' => $request->phone[$i],
                    ]);
                }
            }
        }
        return redirect('/admin/client-find/' . $id)
            ->with('success', 'Профиль успешно обновлен');
    }

    public function clientOrderCreate($id)
    {
        $refArr['refProduct'] = RefProduct::all()->pluck('title', 'id');
        $refArr['refFuelKind'] = RefFuelKind::all()->pluck('title', 'id');
        $refArr['refDeliveryKind'] = RefDeliveryKind::all()->pluck('title', 'id');
        $refArr['refPayKind'] = RefPayKind::all()->pluck('title', 'id');

        $user = User::find($id);

        if (isset($user->userFuelCard)) {

            $cards = UserFuelCard::where('user_id', $id)->get();

            $refArrJSON = json_encode($refArr, JSON_UNESCAPED_UNICODE);
            $cardsArrJSON = json_encode($cards, JSON_UNESCAPED_UNICODE);


            return view('admin.clients.clientsFind.clientsOrder.create',
                compact('user', 'refArrJSON', 'cardsArrJSON', 'id'));
        } else {
            echo 'У клиента нет топливной карты';
        }
    }

    public function clientOrderStore(StoreClientOrder $request, $id)
    {
        Order::create([
            'user_id' => $id,
            'product' => $request->product,
            'user_fuel_card_id' => $request->fuel_card,
            'fuel_kind' => $request->fuel_kind,
            'fuel_amount' => $request->fuel_amount,
            'fuel_price' => $request->fuel_price,
            'fuel_cost' => $request->fuel_cost,
            'delivery_kind' => $request->delivery_kind,
            'delivery_address' => $request->delivery_address,
            'pay_kind' => $request->pay_kind,
            'comment' => $request->comment,
        ]);

        return redirect('/admin/client-find/' . $request->id)
            ->with('success', 'Заказ успешно добавлен');
    }

    public function clientOrderEdit($idOrder)
    {
        $refArr['refStatus'] = RefStatus::all()->pluck('title', 'id');
        $refArr['refProduct'] = RefProduct::all()->pluck('title', 'id');
        $refArr['refFuelKind'] = RefFuelKind::all()->pluck('title', 'id');
        $refArr['refDeliveryKind'] = RefDeliveryKind::all()->pluck('title', 'id');
        $refArr['refPayKind'] = RefPayKind::all()->pluck('title', 'id');

        $order = Order::find($idOrder);
        $id = $order->user_id;

        $user = User::find($id);

        if (isset($user->userFuelCard)) {

            $cards = UserFuelCard::where('user_id', $id)->get();

            $refArrJSON = json_encode($refArr, JSON_UNESCAPED_UNICODE);
            $orderJSON = json_encode($order, JSON_UNESCAPED_UNICODE);
            $cardsArrJSON = json_encode($cards, JSON_UNESCAPED_UNICODE);


            return view('admin.clients.clientsFind.clientsOrder.edit',
                compact('orderJSON', 'refArrJSON', 'idOrder', 'user', 'cardsArrJSON'));
        } else {
            echo 'У клиента нет топливной карты';
        }
    }


    public function clientOrderUpdate(StoreClientOrder $request, $idOrder)
    {

        Order::find($idOrder)->update([
            'date' => $request->date,
            'user_id' => $request->id,
            'product' => $request->product,
            'user_fuel_card_id' => $request->fuel_card,
            'fuel_kind' => $request->fuel_kind,
            'fuel_amount' => $request->fuel_amount,
            'fuel_cost' => $request->fuel_cost,
            'delivery_kind' => $request->delivery_kind,
            'delivery_address' => $request->delivery_address,
            'pay_kind' => $request->pay_kind,
            'status' => $request->status,
            'comment' => $request->comment,
        ]);

        return redirect('/admin/client-find/' . $request->id)
            ->with('success', 'Данные успешно обновлены');
    }

    public function clientOrderDelete($idOrder)
    {
        Order::find($idOrder)->delete();

        return redirect()->back()
            ->with('success', 'Данные успешно удалены');
    }

    public function clientCreate()
    {
        $refArr['refFuelKind'] = RefFuelKind::all()->pluck('title', 'id');

        $refArrJSON = json_encode($refArr, JSON_UNESCAPED_UNICODE);


        return view('admin.clients.clientsCreate.index', compact('user', 'refArrJSON', 'cardsArrJSON'));
    }

    public function clientCreateStore(StoreClientProfile $request)
    {
        $new = User::create([
            'name' => $request->name,
            'login' => $request->login,
            'email' => $request->email,
            'info' => $request->info,
            'password' => $request->password,
            'coworker' => 1,
            'admin' => 1,
        ]);

        $id = $new->id;


        if ($request->fuel) {
            $countFuel = count($request->fuel);
            for ($i = 1; $i <= $countFuel; $i++) {
                UserFuelCard::Create([
                    'user_id' => $id,
                    'fuel' => $request->fuel[$i - 1],
                    'card' => $request->card[$i - 1]
                ]);
            }
        }
        if ($request->phone) {
            $countTel = count($request->phone);
            for ($i = 1; $i <= $countTel; $i++) {
                UserPhone::Create([
                    'user_id' => $id,
                    'phone' => $request->phone[$i - 1],
                ]);
            }
        }

        return redirect('/admin/client-create')
            ->with('success', 'Пользователь успешно добавлен');
    }


    public function clientPayCreate($idOrder)
    {
        $refArr['refPayKind'] = RefPayKind::all()->pluck('title', 'id');

        $order = Order::find($idOrder);

        $refArrJSON = json_encode($refArr, JSON_UNESCAPED_UNICODE);
        $orderJSON = json_encode($order, JSON_UNESCAPED_UNICODE);

        return view('admin.clients.clientsFind.clientsPay.index', compact('refArrJSON', 'orderJSON', 'idOrder'));
    }

    public function clientPayStore(StoreClient_Pay $request)
    {
        Pay::create([
            'order_id' => $request->order_id,
            'date' => $request->date,
            'pay_amount' => $request->pay_amount,
            'pay_kind' => $request->pay_kind,
            'payer' => $request->payer,
            'bank' => $request->bank,
        ]);


        return redirect('/admin/client-find/' . $request->user_id)
            ->with('success', 'Оплата успешно создана');
    }

    public function fuelCostCount(Request $request)
    {
        $product = (int)$request->product;
        $volumeGas = (int)$request->volumeGas;
        $fuelCard = (int)$request->fuelCard;
        $fuelKind = (int)$request->fuelKind;

        if ($product == 1) {

            $arr = FuelPriceNew::whereNull('user_fuel_card_id')->where('fuel_kind', $fuelKind)
                ->pluck('fuel_price', 'volume_min')->toArray();


            foreach ($arr as $key => $value) {
                if ($key > $volumeGas) {
                    unset($arr[$key]);
                }
            }

            $maxKey = max(array_keys($arr));

            $result['fuelCost'] = round($arr[$maxKey] * $volumeGas, 2);
            $result['fuelPrice'] = round($arr[$maxKey], 2);

        } elseif ($product == 2) {

            if (FuelPriceNew::where('user_fuel_card_id', $fuelCard)->count()) {

                $arr = FuelPriceNew::where('user_fuel_card_id', $fuelCard)->pluck('fuel_price', 'volume_min')->toArray();

            } else {

                $fuelKind = UserFuelCard::find($fuelCard)->fuel;

                $arr = FuelPriceNew::whereNull('user_fuel_card_id')->where('fuel_kind', $fuelKind)
                    ->pluck('fuel_price', 'volume_min')->toArray();
            }

            foreach ($arr as $key => $value) {
                if ($key > $volumeGas) {
                    unset($arr[$key]);
                }
            }

            $maxKey = max(array_keys($arr));

            $result['fuelCost'] = round($arr[$maxKey] * $volumeGas, 2);
            $result['fuelPrice'] = round($arr[$maxKey], 2);
        }


        return $result;
    }

    public function fuelCostCountByPrice(Request $request)
    {
        try {
            $volumeGas = (int)$request->volumeGas;
            $fuelPrice = (float)$request->fuelPrice;
            $result['fuelCost'] = round($fuelPrice * $volumeGas, 2);

            return $result;
        } catch (\Exception $e) {
            return 0;
        }
    }
}
