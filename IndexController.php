<?php

namespace App\Http\Controllers;

use App\Models\Param;
use App\Models\RealParam;
use App\Models\Ref\ParamType;
use App\Models\Ref\Ref;
use App\Models\Value;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class IndexController extends Controller
{
    //формирует главную страницу
    public function index(Request $request)
    {
        $refs = Ref::order()->pluck('title', 'id');
        $params = Param::pluck('title', 'id');
        $paramType = ParamType::pluck('title', 'id');
        $realParams = RealParam::pluck('title', 'id');

        return view('index', [
            'refs' => $refs,
            'params' => $params,
            'realParams' => $realParams,
            'paramType' => $paramType,
        ]);
    }

    //возвращает значения массива синонимов
    public function synonymsValues(Request $request)
    {
        if (isset($request->synonyms)) {
            $t = [];
            foreach ($request->synonyms as $k => $v) {
                DB::statement("SET SESSION group_concat_max_len = 4000000000;");

                $t[$v] = Value::selectRaw("
                `values`.`value` AS `value`,
                `values`.`real_value` AS `real_value`,
                `values`.`dimension` AS `dimension`,
                `values`.`param_id` AS `param_id`,
                GROUP_CONCAT(`values`.id separator ',') AS `idArr`
                ")
                    ->where('param_id', $k)
                    ->groupBy('dimension', 'value', 'param_id', 'real_value')->get()->toArray();

                foreach ($t[$v] as &$val) {
                    $val['idArr'] = explode(',', $val['idArr']);
                }
            }
            $result = $t;
        } else {
            $result = [];
        }
        unset($t);

        return $result;
    }

    //возвращает массив данных для открытия реального параметра
    public function realParamEditArr(Request $request)
    {
        $refIdChosen = RealParam::find($request->id)->ref_id;
        $refValuesChosen = \App\Models\Ref\Value::where('ref_id', $refIdChosen)->pluck('value', 'id')->toArray();
        $refValuesChosen = array(null => 'нет') + $refValuesChosen;
        $paramType = RealParam::find($request->id)->ref_param_type_id;
        $synonyms = Param::where('real_param_id', $request->id)->pluck('title', 'id')->toArray();

        $t = [];

        DB::statement("SET SESSION group_concat_max_len = 4000000000;");

        $synonymsArray = Param::where('real_param_id', $request->id)->pluck('title', 'id')->toArray();

        foreach ($synonymsArray as $paramId => $title) {

            $t[$title] = Value::selectRaw("
                    `values`.`value` AS `value`,
                    `values`.`real_value` AS `real_value`,
                    `values`.`dimension` AS `dimension`,
                    `values`.`param_id` AS `param_id`,
                    GROUP_CONCAT(`values`.id separator ',') AS `idArr`")
                ->leftJoin('param', 'values.param_id', '=', 'param.id')
                ->where('param_id', $paramId)
                ->groupBy('dimension', 'value', 'param_id', 'real_value')->get()->toArray();


            foreach ($t[$title] as &$val) {
                $val['idArr'] = explode(',', $val['idArr']);
            }
        }

        $synonymsValues = $t;

        unset($t);

        return [
            'refIdChosen' => $refIdChosen,
            'refValuesChosen' => $refValuesChosen,
            'paramType' => $paramType,
            'synonyms' => $synonyms,
            'synonymsValues' => $synonymsValues
        ];
    }

    //возвращает url товара
    public function idToUrl(Request $request)
    {
        if (isset($request->id)) {
            $url = Value::find($request->id)->product()->first()->url;
        } else {
            $url = null;
        }
        return $url;
    }

    //возвращает старые значения параметра для просмотра
    public function value(Request $request)
    {
        if (isset($request->param_id)) {
            DB::statement("SET SESSION group_concat_max_len = 4000000000;");

            $values = Value::selectRaw("
            `values`.`value` AS `value`,
            `param_id` AS `param_id`,
            group_concat(`values`.
            `product_id` separator ',') AS `idArr`")
                ->where('param_id', $request->param_id)
                ->groupBy('value', 'param_id')->get()->toArray();

            foreach ($values as &$v) {
                $v['idArr'] = explode(',', $v['idArr']);
            }

        } else {
            $values = [];
        }
        return $values;
    }

    //обновляет или создает реальный параметр
    public function realParamUpdate(Request $request)
    {
        if ($request->valuesIdArr) {
            $realParam = RealParam::updateOrCreate([
                'id' => $request->realParamChosenId
            ], [
                'title' => $request->paramTitle,
                'ref_param_type_id' => $request->paramType,
                'ref_id' => $request->refIdChosen
            ]);

            $paramIdArr = explode(',', $request->paramIdArr);


            Param::whereIn('id', $paramIdArr)->update([
                'real_param_id' => $realParam->id
            ]);

            foreach ($request->valuesIdArr as $k => $valuesIdArr) {
                $valuesIdArr = explode(',', $valuesIdArr);

                if ($request->realValue[$k]) {
                    Value::whereIn('id', $valuesIdArr)->update([
                        'real_value' => $request->realValue[$k],
                        'real_dimension' => $request->realDimension,

                    ]);
                }

                if (isset($request->refId[$k])) {
                    Value::whereIn('id', $valuesIdArr)->update([
                        'real_value' => $request->refId[$k],
                    ]);
                }

            }

        }
        return redirect()->back();
    }
}

